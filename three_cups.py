from random import shuffle

def shuffle_cups(cups_list):
	shuffle(cups_list)
	return cups_list

def player_input():
	pl_inp = ''
	while pl_inp not in ['0','1','2']:
		pl_inp = input('Enter your choice (0,1 or 2) : ')
	return int(pl_inp)

def player_output(cups_list, pl_inp):
	if cups_list[pl_inp] == 'O':
		return True
	else:
		return False

cups_list = [' ','O',' ']

shuffled_cups = shuffle_cups(cups_list)
player_choice = player_input()
if player_output(shuffled_cups, player_choice) == True:
	print("you WON!!!")
	print(shuffled_cups)
else:
	print('YOU LOSE')
	print(shuffled_cups)
