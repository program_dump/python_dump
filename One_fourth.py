# given an input string, return the string with first and fourth letters capitalised

def one_fourth(a_string):
    final_string = ''
    for index_val, letter in enumerate(a_string):
        if index_val == 0 or index_val == 3:
            final_string += a_string[index_val].upper()
        else:
            final_string += a_string[index_val].lower()
    return final_string

var_a = input('Enter your value (:string only)')
print(one_fourth(var_a))
