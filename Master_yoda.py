# return the given string in reverse order

def master_yoda(a_string):
    split_string = a_string.split()
    return ' '.join(split_string[::-1])

var_a = input('Enter a value(:string) ')
print(master_yoda(var_a))
