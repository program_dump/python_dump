# given a list as an input, return true if '007' integers are aligned in order

def james_bond(inp_list):
    final_list = []
    final_sort_list = []
    for number in inp_list:
        if number == 0 or number == 7:
            final_list.append(number)
            final_sort_list.append(number)
    final_sort_list.sort()
    if final_list == final_sort_list:
        return True
    else:
        return False

a_var = james_bond([1,2,0,0,6,7,9])
b_var = james_bond([0,7,6,5,0,4,1])
print(a_var)
print(b_var)
