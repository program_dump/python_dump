# return the output string with same letters repeating thrice

def paper_doll(inp_string):
    ind_val = 0
    final_string = ''
    while ind_val < len(inp_string):
        for index_val, letter in enumerate(inp_string):
            final_string += letter*3
            ind_val += 1
    return final_string

var_a = input('Enter some Value (string or int) :')
print(paper_doll(var_a))
