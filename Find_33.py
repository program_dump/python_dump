# given a list, return true if a 3 is beside another 3, else return false

def find_33(a_list):
    final_string = ''
    for elem in a_list:
        final_string += str(elem)
    if '33' in final_string:
        return True
    else:
        return False

def accept_input(var_b):
    accept_list = True
    while accept_list:
        try:
            li = list(var_b.split())
            return li
        except TypeError:
            print("ENTER ONLY NUMBERS!")

var_b = input('Enter numbers: ')
final_list = accept_input(var_b)
print(find_33(final_list))
