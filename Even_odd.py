# if both the numbers are even return the lesser number, if any one of the number is odd return the greater number between the two

def even_odd(a,b):
    if a % 2 == 0 and b % 2 == 0:
        return min(a,b)
    else:
        return max(a,b)
var_a = even_odd(2,3)
print(var_a)
