# input is the range, output should be the total number of prime numbers upto and including the number given

def prime_number(inp_int):
    prime_list = []
    for number in range(inp_int+1):
        if number > 1:
            for num in  range(2, number):
                if number % num == 0:
                    break
            else:
                prime_list.append(number)
    return len(prime_list), prime_list

a_var = prime_number(100)
print(a_var)
