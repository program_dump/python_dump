def summer_34(list_34):
    fin_sum = 0
    skip = False
    for elem in list_34:
        if elem == 3:
            skip = True
            continue
        if elem == 4:
            skip = False
            continue
        if skip == False:
            fin_sum += elem
    return fin_sum
var_a = summer_34([2, 1, 3, 4, 11])    
var_b = summer_34([1, 5, 3, 7, 8, 4])
print(var_a)
print(var_b)
